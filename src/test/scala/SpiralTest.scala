package cz.cvut.fit.dosoudom

import org.scalatest.flatspec.AnyFlatSpec

class SpiralTest extends AnyFlatSpec {
  it should "generate simple spiral" in {
    assert(spiral(1).toString == "*")
  }

  it should "generate small spiral" in {
    assert(
      spiral(2).toString ==
        """**
              |* """.stripMargin
    )
    assert(
      spiral(3).toString ==
        """***
          |* *
          |  *""".stripMargin
    )
    assert(
      spiral(4).toString ==
        """ ***
          | * *
          |   *
          |****""".stripMargin
    )
    assert(
      spiral(5).toString ==
        """*    
          |* ***
          |* * *
          |*   *
          |*****""".stripMargin
    )
    assert(
      spiral(6).toString ==
        """******
          |*     
          |* *** 
          |* * * 
          |*   * 
          |***** """.stripMargin
    )
    assert(
      spiral(7).toString ==
        """*******
          |*     *
          |* *** *
          |* * * *
          |*   * *
          |***** *
          |      *""".stripMargin
    )
    assert(
      spiral(8).toString ==
        """ *******
          | *     *
          | * *** *
          | * * * *
          | *   * *
          | ***** *
          |       *
          |********""".stripMargin
    )
  }
}
