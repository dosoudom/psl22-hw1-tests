Add following dependencies to `build.sbt`:

```aidl
libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.13"
libraryDependencies += "org.scalatest" %% "scalatest" % "latest.integration" % "test"
```

and update package paths (or function/class names) in the tests.